from django.urls import path

from web.views import main_view
from web.views import registration_view, auth_view, logout_view, build_edit_view

urlpatterns = [
    path('', main_view, name="main"),
    path('registration/', registration_view, name="registration"),
    path('auth/', auth_view, name="auth"),
    path('logout/', logout_view, name="logout"),
    path('builds/add/', build_edit_view, name='build_add'),
    path('builds/<int:id>/', build_edit_view, name='build_edit')
]
