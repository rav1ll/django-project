from django.db import models
from django.contrib.auth import get_user_model

# Create your models here.

User = get_user_model()


class UserBuild(models.Model):
    name = models.CharField(max_length=100)
    rating = models.IntegerField
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='builds_pics/', null=True, blank=True)
    description = models.CharField(max_length=256, default=' ')


class Part(models.Model):
    name = models.CharField(max_length=100)
    price = models.IntegerField
    specs = models.CharField(max_length=150)
    build = models.ManyToManyField(UserBuild)
    image = models.ImageField(upload_to='parts_pics/', null=True, blank=True)
