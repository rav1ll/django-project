# Generated by Django 4.1.5 on 2023-05-13 17:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='part',
            name='Image',
            field=models.ImageField(blank=True, null=True, upload_to='builds_pics/'),
        ),
    ]
