from datetime import datetime

from django.shortcuts import render, redirect, get_object_or_404
from web.forms import RegistrationForm, AuthForm, UserBuildForm

from web.models import UserBuild, Part

from django.contrib.auth import get_user_model, authenticate, login, logout
from django.shortcuts import redirect

User = get_user_model()


# Create your views here.


def main_view(request):
    year = datetime.now().year

    builds = UserBuild.objects.all()

    return render(request, 'web/main.html',
                  {
                      'year': year,
                      'builds': builds
                  })


def registration_view(request):
    form = RegistrationForm()
    is_success = False
    if request.method == 'POST':
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = User(
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email'],
            )
            user.set_password(form.cleaned_data['password'])
            user.save()
            is_success = True
    return render(request, "web/registration.html", {
        "form": form, "is_success": is_success
    })


def auth_view(request):
    form = AuthForm()
    if request.method == 'POST':
        form = AuthForm(data=request.POST)
        if form.is_valid():
            user = authenticate(**form.cleaned_data)
            if user is None:
                form.add_error(None, "Введены неверные данные")
            else:
                login(request, user)
                return redirect("main")
    return render(request, "web/auth.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("main")


def build_edit_view(request, id=None):
    build = get_object_or_404(UserBuild, user=request.user, id=id) if id is not None else None
    form = UserBuildForm(instance=build)
    if request.method == 'POST':
        form = UserBuildForm(data=request.POST, files=request.FILES, instance=build, initial={"user": request.user})
        if form.is_valid():
            form.save()
            return redirect("main")
    return render(request, "web/build_add.html", {"form": form})
